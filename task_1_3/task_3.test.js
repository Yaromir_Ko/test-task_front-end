const getParamsFromURL = require('./task_3')

describe('The function that parses request parameters from a URL string and transforms it into an object with the appropriate key and value.', () => {

    const URL_1 = "http://localhost:63342/test/index.html?em=world&ew=book";
    const ANSWER_1 = {
        'em': 'world',
        'ew': 'book'
    }
    const URL_2 = "https://www.google.com/search?q=random&oq=random+&sourceid=chrome&ie=UTF-8";
    const ANSWER_2 = {
        'q': 'random',
        'oq': 'random ',
        'sourceid': 'chrome',
        'ie': 'UTF-8'
    }


    it('first check', () => {
        expect(getParamsFromURL(URL_1)).toMatchObject(ANSWER_1)
    })
    it('second check', () => {
        expect(getParamsFromURL(URL_2)).toMatchObject(ANSWER_2)
    })

})

