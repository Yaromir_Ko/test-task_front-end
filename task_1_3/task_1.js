function findFrequentCharacter (str) {
    const tmp = new Map()
    let biggerNum = 0
    let name = null

    for (let elem of str.split(' ').join('').toLowerCase()) {

        let value = tmp.get(elem)

        if (value) {
            const val = ++value
            tmp.set(elem, val)

            if (val > biggerNum) {
                biggerNum = val
                name = elem
            }
            continue
        }
        tmp.set(elem, 1)
    }
    return name
}

module.exports = findFrequentCharacter
