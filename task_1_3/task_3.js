function getParamsFromDocumentLocation() {
    const params = new URL(document.location)
    return Object.fromEntries(params.searchParams)
}

function getParamsFromURL(url) {
    const params = new URL(url)
    return Object.fromEntries(params.searchParams)
}

module.exports = getParamsFromURL
