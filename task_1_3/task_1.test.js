const findFrequentCharacter = require('./task_1')

describe('The function that find the most frequent character in a string (case insensitive)', () => {

    const STR_1 = 'ASASSSASAsaasaBBBASvcdNNSASASxxzccxcv'
    const ANSWER_1 = 's'
    const STR_2 = 'SaLJlllLnAnnnNNNopo'
    const ANSWER_2 = 'n'
    const STR_3 = 'Grass is greener on the other side'
    const ANSWER_3 = 'e'

    it.each`
      str      | expected
      ${STR_1} | ${ANSWER_1}
      ${STR_2} | ${ANSWER_2}
      ${STR_3} | ${ANSWER_3}
    `('returns $expected for $str', ({str, expected}) => {
        expect(findFrequentCharacter(str)).toBe(expected)
    });

    it.each`
      str      | notExpected
      ${STR_1} | ${'n'}
      ${STR_1} | ${'z'}
      ${STR_1} | ${'v'}
      
      ${STR_2} | ${' '}
      ${STR_2} | ${'a'}
      ${STR_2} | ${'Q'}
      
      ${STR_3} | ${'o'}
    `('returns $notExpected for $str', ({str, notExpected}) => {
        expect(findFrequentCharacter(str)).not.toBe(notExpected)
    });

})

