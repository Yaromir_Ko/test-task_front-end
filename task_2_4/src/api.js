const URL = "https://api.y-media.io/testjson/task";

const loadProducts = async () => {
  const response = await fetch(URL);
  return await response.json();
};

export { loadProducts };
