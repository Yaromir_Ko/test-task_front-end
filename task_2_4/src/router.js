import VueRouter from "vue-router";
import Vue from "vue";

import Task_2 from "./components/Task_2";
import Task_4 from "./components/Task_4";

const routes = [
  { path: "/", component: Task_2 },
  { path: "/task_4", component: Task_4 },
];

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes,
});
