import { mount } from "@vue/test-utils";
import App from "@/App.vue";

describe("App component", () => {
  let wrapper;

  const createComponent = () => {
    wrapper = mount(App, {
      stubs: ["router-link", "router-view"],
    });
  };

  afterEach(() => {
    wrapper.destroy();
  });

  it("renders two buttons on page", () => {
    createComponent();
    const BUTTON = "BUTTON";

    const buttons = wrapper.findAll("button");

    expect(buttons.at(0).element.tagName).toBe(BUTTON);
    expect(buttons.at(1).element.tagName).toBe(BUTTON);
  });
});
