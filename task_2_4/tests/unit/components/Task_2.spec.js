import { shallowMount } from "@vue/test-utils";
import Task_2 from "@/components/Task_2.vue";

describe("Task_2.vue", () => {
  let wrapper;

  const createComponent = () => {
    wrapper = shallowMount(Task_2);
  };

  afterEach(() => {
    wrapper.destroy();
  });

  it("renders two buttons ['Set', 'Clear'] and the input field on page", () => {
    createComponent();

    const BUTTON_SET = "Set";
    const BUTTON_CLEAR = "Clear";
    const INPUT = "INPUT";

    const buttons = wrapper.findAll("button");

    expect(wrapper.find("input").element.tagName).toBe(INPUT);
    expect(buttons.at(0).text()).toBe(BUTTON_SET);
    expect(buttons.at(1).text()).toBe(BUTTON_CLEAR);
  });

  it("the button 'Set' is disabled when initialized", () => {
    createComponent();

    expect(wrapper.find("button").element.disabled).toBe(true);
  });

  it("the input field type is number", () => {
    createComponent();

    expect(wrapper.find('input[type="number"]').exists()).toBe(true);
  });

  it("when the input field is not empty the Set button is active", async () => {
    createComponent();
    const NEW_VALUE = 34;
    const input = wrapper.find('input[type="number"]');
    await input.setValue(NEW_VALUE);
    expect(wrapper.find("button").element.disabled).toBe(false);
  });

  it("when set to even number green background", async () => {
    createComponent();
    const NEW_VALUE = 34;
    const CLASS_NAME = "bg-green";
    const input = wrapper.find('input[type="number"]');
    await input.setValue(NEW_VALUE);
    await wrapper.find("button").trigger("click");
    expect(wrapper.find("p").element.className).toBe(CLASS_NAME);
  });

  it("when set to odd number red background", async () => {
    createComponent();
    const NEW_VALUE = 33;
    const CLASS_NAME = "bg-red";
    const input = wrapper.find('input[type="number"]');
    await input.setValue(NEW_VALUE);
    await wrapper.find("button").trigger("click");
    expect(wrapper.find("p").element.className).toBe(CLASS_NAME);
  });

  it("when the clear button is pressed, the paragraph is hidden", async () => {
    createComponent();
    const NEW_VALUE = 33;
    const input = wrapper.find('input[type="number"]');
    await input.setValue(NEW_VALUE);
    await wrapper.find("button").trigger("click");
    expect(wrapper.find("p").html()).toContain(NEW_VALUE);

    await wrapper.findAll("button").at(1).trigger("click");

    expect(wrapper.html()).not.toContain(NEW_VALUE);
  });
});
